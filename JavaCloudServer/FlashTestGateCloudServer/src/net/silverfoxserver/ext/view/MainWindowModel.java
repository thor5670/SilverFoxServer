/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.silverfoxserver.ext.view;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import net.silverfoxserver.core.socket.SocketAcceptor;
import net.silverfoxserver.core.socket.SocketConnector;

/**
 *
 * @author ACER-FX
 */
public class MainWindowModel {
    
    public SocketAcceptor acceptor; 
    
    private final ConcurrentHashMap _connectServerList;
    
    public MainWindowModel()
    {
        _connectServerList = new ConcurrentHashMap();
    
    }
    
    public ConcurrentHashMap getConnectServerList()
    {
    
        return _connectServerList;
    
    }
    
    public SocketConnector findConnectServer(String value)
    {
        /*
        int len = _connectServerList.size();
        
        for(int i=0;i<len;i++)
        {
            SocketConnector s = _connectServerList.get(i);
            
            if(s.getConnectServerName().equals(value))
            {
                return s;
            }
        
        }
        */
        
        if(_connectServerList.containsKey(value))
        {
            return (SocketConnector)_connectServerList.get(value);
        }
    
        return null;
    }
    
    /**
     * 
     * 
     * @param value 
     */
    public void retryConnect(String value)
    {
        
        SocketConnector s = this.findConnectServer(value);
        
        if(s != null){
        
            s.retryConnect();
        
        }
    
    }
    
    
}
