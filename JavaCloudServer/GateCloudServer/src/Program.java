/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import net.silverfoxserver.ext.Globals;
import System.Console;
import System.ConsoleColor;
import System.IO.Path;
import System.Xml.XmlDocument;
import System.Xml.XmlNode;
import ch.makery.address.model.Person;
import ch.makery.address.view.PersonEditDialogController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.silverfoxserver.ext.socket.SocketConnector4;
import net.silverfoxserver.ext.socket.WebSocketAcceptor;
import net.silverfoxserver.ext.util.SR;
import org.jdom2.Element;
import org.jdom2.JDOMException;

/**
 *
 * @author ACER-FX
 */
public class Program extends Application {
    
        
    @Override
    public void start(Stage stage) throws Exception {
        
        //
        Globals.stage = stage;
        
        //os            
        String os = System.getProperty("os.name");
            
        Globals.os = os;
        
        //lang
        Locale machine = Locale.getDefault();
        String lang = machine.getLanguage() + "-" + machine.getCountry();//"zh-CN"; // System.Globalization.CultureInfo.InstalledUICulture.Name;

        Globals.lang = lang;
        
        SR.init(lang);
        
        //load MainWindow.fxml     
        Parent root = FXMLLoader.load(getClass().getResource("/net/silverfoxserver/ext/view/MainWindow.fxml"));//"FXMLDocument.fxml"));
                
        Scene scene = new Scene(root);
        
        stage.setTitle("Gate Service (build date:2016/8/9)");
        
        // Set the application icon.
        //stage.getIcons().add(new Image("file:resources/favicon.ico"));
    
        stage.setScene(scene);
        stage.show();
               
        
        //
        loadXMLConfigAndStart();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    
    //public void loadXMLConfig()
    public void loadXMLConfigAndStart()
    {
        
        try {
    //load xml config
        //读取xml配置
        XmlDocument configDoc = new XmlDocument();

        //获取和设置当前目录（即该进程从中启动的目录）的完全限定路径。
        //string str = System.Environment.CurrentDirectory;
        //该程序如果由另一程序启动，则会有问题，获得的是另一程序的启动路径

        //
        String configFileName = "GateCloudServerConfig.xml";

        //
        String ApplicationBase = "";//System.getProperty("user.dir");

        if(Globals.os.equals("Linux"))
        {
            //在Linux下面用这个方法,获得执行jar的运行路径
            String javaClassPath = System.getProperty("java.class.path");
            ApplicationBase = javaClassPath.substring(0,
                    javaClassPath.lastIndexOf("/")
                    );

        }else{

            ApplicationBase = System.getProperty("user.dir");
        }

        //            
        String configFileFullPath = Path.Combine(ApplicationBase, configFileName);

        //
        //Console.WriteLine("[Load File] " + System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + configFileName);

        Globals.out.println(SR.GetString(SR.getLoadFile(), configFileFullPath));
        
        
        configDoc.Load(configFileFullPath);
        


        //IP信息
        XmlNode node = configDoc.SelectSingleNode("/SilverFoxConfig/group/main-server");

        if (null == node)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Globals.out.println(SR.GetString(SR.getCan_not_find_node(),"/SilverFoxConfig/group/main-server"));
            Console.ForegroundColor = ConsoleColor.Green;
        }
                
        
        //
        String ipAdr = node.ChildNodes()[0].getText();

        int port = Integer.parseInt(node.ChildNodes()[1].getText());
        
        String mode = node.ChildNodes()[2].getText();
        
        Globals.mainWindowModel.setPort(String.valueOf(port));
        Globals.mainWindowModel.setMode(mode);
        
        
        //
        XmlNode connectServersNode = configDoc.SelectSingleNode("/SilverFoxConfig/group/connect-servers");//game-server
        
        int connectServersCount = connectServersNode.ChildNodes().length;
        
        ArrayList<SocketConnector4> connectServerList = new ArrayList<SocketConnector4>() {};
        
        for(int i=0;i<connectServersCount;i++)
        {
            
            Element gameServerNode = connectServersNode.ChildNodes()[i];
            
            String connectIp = gameServerNode.getChildText("ip");             
            
            int connectPort = Integer.parseInt(gameServerNode.getChildText("port"));
            String connectProof = gameServerNode.getChildText("proof");
            
            SocketConnector4 connector = new SocketConnector4(connectProof);
        
            connectServerList.add(connector);
            
            connector.connect(connectIp, connectPort);
        }
        
        //------------------------------------------------------
        Globals.out.println(SR.GetString(SR.getLoadFileSuccess()));
        
        
        
        
        WebSocketAcceptor acceptor = new WebSocketAcceptor();
        
        //最后启动外网侦听
        acceptor.bind(port, false);
        
        
        
        } catch (JDOMException | IOException ex) {
            //Logger.getLogger(Program.class.getName()).log(Level.SEVERE, null, ex);
            
            Globals.out.println(ex.getMessage());
            
        }
    
    
    }
 

    
}
