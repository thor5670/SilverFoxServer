package net.silverfoxserver.ext;


import javafx.stage.Stage;
import net.silverfoxserver.ext.view.MainWindowController;
import net.silverfoxserver.ext.view.MainWindowModel;

/*
 * SilverFoxServer: massive multiplayer game server for h5, ...
 * VERSION:3.0
 * PUBLISH DATE:2015-9-2 
 * GITHUB:github.com/wdmir/521266750_qq_com
 * UPDATES AND DOCUMENTATION AT: http://www.silverfoxserver.net
 * COPYRIGHT 2009-2015 SilverFoxServer.NET. All rights reserved.
 * MAIL:521266750@qq.com
 */




/**
 *
 * @author ACER-FX
 */
public class Globals {
    
    /**
     * os name
     * 
     */
    public static String os;
    
    /**
     * 
     * lang
     */
    public static String lang; 
    
    
    /**
     * javafx root stage
     */
    public static Stage stage;
    
    
    /**
     * 
     */
    public static MainWindowController mainWindowController;
    
    /**
     * 
     */
    public static MainWindowModel mainWindowModel;
    
    
    public static Out out = new Out();
    
    
    
    
    
    
   
    
}
