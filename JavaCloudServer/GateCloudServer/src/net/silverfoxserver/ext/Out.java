/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.silverfoxserver.ext;

import java.time.LocalDate;
import java.time.LocalTime;
import javafx.application.Platform;
import net.silverfoxserver.ext.util.SR;

/**
 *
 * @author ACER-FX
 */
public class Out {
    
    
    
    
    public void println(String value)
    {
        
        
        System.out.println(value);
        
        Globals.mainWindowModel.println(value);

       
        
    
                
        
    }
    
    
    public void WriteStrByException(String className, String funcName, String cause)
    {
        
        String value = String.format("[%1$s:%2$s:%3$s,%4$s] %5$s:%6$s %7$s:%8$s %9$s:%10$s", 
                            getHour(), getMinute(), getSecond(), getMillisecond(),
                            SR.getExce_p_tion(), className, SR.getFunc_tion(), funcName, SR.getCasue(), cause);
        
        
        System.err.println(value);

        Globals.mainWindowModel.println(value);
    }
    
    
    
    public static String getYear()
    {   
            int year =  LocalDate.now().getYear();

            return String.valueOf(year);
    }

    public static String getMonth()
    {
            int month = LocalDate.now().getMonthValue();
            
            String monthStr = String.valueOf(month);

            if (1 == monthStr.length())
            {
                    monthStr = "0" + monthStr;
            }

            return monthStr;
    }

    public static String getDay()
    {
            int day = LocalDate.now().getDayOfMonth();

            String dayStr = String.valueOf(day);
            
            if (1 == dayStr.length())
            {
                    dayStr = "0" + dayStr;
            }

            return dayStr;

    }

    public static String getHour()
    {
            int hour = LocalTime.now().getHour();

            String hourStr = String.valueOf(hour);
            
            if (1 == hourStr.length())
            {
                    hourStr = "0" + hourStr;
            }

            return hourStr;
    }

    public static String getMinute()
    {
        
            int minute = LocalTime.now().getMinute();

            String minuteStr = String.valueOf(minute);
            
            if (1 == minuteStr.length())
            {
                    minuteStr = "0" + minuteStr;
            }

            return minuteStr;
    }

    public static String getSecond()
    {
            int second = LocalTime.now().getSecond();
            String secondStr = String.valueOf(second);

            if (1 == secondStr.length())
            {
                    secondStr = "0" + secondStr;
            }

            return secondStr;
    }

    public static String getMillisecond()
    {
            int millisecond =  LocalTime.now().getNano();

            String millisecondStr = String.valueOf(millisecond);
            
            if (1 == millisecondStr.length())
            {
                    millisecondStr = "0" + "0" + millisecondStr;
            }
            else if (2 == millisecondStr.length())
            {
                    millisecondStr = "0" + millisecondStr;
            }

            return millisecondStr;
    }
    
    
    
}
