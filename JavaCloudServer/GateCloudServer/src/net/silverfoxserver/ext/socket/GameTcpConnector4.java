/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.silverfoxserver.ext.socket;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 *
 * @author ACER-FX
 */
public class GameTcpConnector4 extends Bootstrap {
    
    
    public GameTcpConnector4()
    {
        
         EventLoopGroup workerGroup = new NioEventLoopGroup();
         
         super.group(workerGroup); // (2)
         super.channel(NioSocketChannel.class); // (3)
         super.option(ChannelOption.SO_KEEPALIVE, true); // (4)
         super.handler(new GameTcpConnector4ChannelInitializer());
         
    
    }
    
    
    
    
}
