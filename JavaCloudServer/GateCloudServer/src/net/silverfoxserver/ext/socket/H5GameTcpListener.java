/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.silverfoxserver.ext.socket;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER-FX
 */
public class H5GameTcpListener extends ServerBootstrap{
        
    private int _port = 0;    
    
    public boolean Setup(int port)
    {
        _port = port;
        
        return true;//isLoclePortUsing(port);
    }
    
    public void Start()
    {
        
        EventLoopGroup bossGroup = new NioEventLoopGroup(); // (1)
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        
        //try {
           
            super.group(bossGroup, workerGroup)
             .channel(NioServerSocketChannel.class) // (3)
             .childHandler(new H5GameTcpListenerChannelInitializer())  //(4)
             .option(ChannelOption.SO_BACKLOG, 128)          // (5)
             .childOption(ChannelOption.SO_KEEPALIVE, true); // (6)

            System.out.println("H5GameTcpListener Start!");

            // 绑定端口，开始接收进来的连接
            //ChannelFuture f =  super.bind(new InetSocketAddress(_port)).sync(); // (7)
            super.bind(new InetSocketAddress(_port));
            
            // 等待服务器  socket 关闭 。
            // 在这个例子中，这不会发生，但你可以优雅地关闭你的服务器。
            //f.channel().closeFuture().sync();

        //} catch (InterruptedException ex) {
            
            //Logger.getLogger(H5GameTcpListener.class.getName()).log(Level.SEVERE, null, ex);
            
        //    System.out.println("H5GameTcpListener Exception:" + ex.getMessage());
            
        //} 
        //finally {
            
        //    workerGroup.shutdownGracefully();
        //    bossGroup.shutdownGracefully();

        //    System.out.println("H5GameTcpListener Close");
        //}
           
    }
    
    
}
