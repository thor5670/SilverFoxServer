/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.silverfoxserver.ext.socket;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.silverfoxserver.ext.Globals;
import net.silverfoxserver.ext.util.TimeUtil;

/**
 *
 * @author ACER-FX
 */
public class SocketConnector4{
    
    
    /** 
	 要连接的服务器ip
    */
    private String _connect_serverIp = "127.0.0.1";

    /** 
      要连接的服务器的端口
    */
    private int _connect_serverPort = 9500;
    
    /** 
	 连接服务器的证书
    */
    private String _connect_serverProof = "www.silverfoxserver.net";

    public final String getProof()
    {
            return this._connect_serverProof;
    }
    
    /** 

    */
    private GameTcpConnector4 _tcpClient = null;

    public final String getRemoteEndPoint()
    {
            return _connect_serverIp + ":" + _connect_serverPort;
    }
    
    /**
     * 
     * 
     * @param proof 
     */
    public SocketConnector4(String proof)
    {        
            this._connect_serverProof = proof;        
            
            this._tcpClient = new GameTcpConnector4();
    }  
    
    /**
     * 
     * 
     * @param ipAdr
     * @param port 
     */
    public final void connect(String ipAdr, int port)
    {
        
        //
        this._connect_serverIp = ipAdr;
        this._connect_serverPort = port;

        try {   
                
            // Start the connection attempt.
            ChannelFuture future = _tcpClient.connect(new InetSocketAddress(ipAdr, port));

            
            future.addListener(new ChannelFutureListener(){
                
                @Override
                public void operationComplete(ChannelFuture future){
                    
                    Globals.out.println("connect " + ipAdr + ":" + String.valueOf(port) + " " + future.toString());
                
                    boolean open = future.channel().isOpen();
                    
                    if(!open)
                    {
                        ActionListener connectFuncAct = new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                                //...Perform a task...
                                connectFunction();
                            }
                        };

                        TimeUtil.setTimeout(5000, connectFuncAct);
                    
                    
                    }
                    
                    
                    
                }
            });
                    
                    
        
            // Wait until the connection is made successfully.
            //Channel channel = future.sync().channel();
            
            
            //_connect_serverSock = new AppSession(channel);
            
        } catch (java.lang.IllegalStateException ex) {
            
           Globals.out.WriteStrByException(SocketConnector4.class.getName(), "connect", ex.getMessage());
            
        }
                
            
    
    }
    
     public void connectFunction()
     {
         connect(_connect_serverIp,_connect_serverPort);
     
     }
    
    
    
}
