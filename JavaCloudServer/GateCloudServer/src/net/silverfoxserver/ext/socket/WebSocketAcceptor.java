/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.silverfoxserver.ext.socket;

import java.util.concurrent.ConcurrentHashMap;
import net.silverfoxserver.ext.Globals;
import net.silverfoxserver.ext.util.SR;

/**
 *
 * @author ACER-FX
 */
public class WebSocketAcceptor {
    
    /** 

    */
    private int _serverPort = 80;
    
    
    /** 

    */
    private H5GameTcpListener _tcpLis = null;
    
    
    /**
     * 
     * 
     */
    public WebSocketAcceptor()
    {
          
        
        _tcpLis = new H5GameTcpListener();
        
         

    }
    
    public final void bind(int port, boolean reuseAddr)
    {

            //
            //this._serverIp = ipAdr;
            this._serverPort = port;
            
            
            if (_tcpLis.Setup(_serverPort))
            {
                
                
            }
            else
            {
                    //setup failed
                    System.out.println(SR.GetString(SR.getGame_tcp_listen_setup_failed()));

                    throw new IllegalArgumentException(SR.GetString(SR.getGame_tcp_listen_setup_failed()));
            }
            
            
            _tcpLis.Start();
            
            //
            Globals.out.println("------------------------------------------");
            Globals.out.println("Listen Mode: WebSocket" + " Port: " + _serverPort);
            Globals.out.println("------------------------------------------");
    
    }
    
    
    
    
    
    
}
