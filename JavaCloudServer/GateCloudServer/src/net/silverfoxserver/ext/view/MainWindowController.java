/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.silverfoxserver.ext.view;

import net.silverfoxserver.ext.Globals;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import net.silverfoxserver.ext.util.SR;


/**
 * FXML Controller class
 *
 * @author ACER-FX
 */
public class MainWindowController implements Initializable {

    @FXML
    private Label modeLbl;
    
    @FXML
    private Label portLbl;
    
    @FXML    
    private TextArea outTA;
    
    
    
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
      
        Globals.mainWindowController = this;
        
        Globals.mainWindowModel = new MainWindowModel();
        
        this.installModel( Globals.mainWindowModel);
        
    }    
    
    public void installModel(MainWindowModel value)
    {
        
       
        value.outProperty().addListener(new ChangeListener(){
            @Override 
            public void changed(ObservableValue o,Object oldVal, Object newVal){
                 //System.out.println("Electric bill has changed!");
                 
                 outTA.setText(value.getOut());
                 
                 //outTA.appendText(null);
            }
      });

        
        
        value.portProperty().addListener(new ChangeListener(){
            @Override 
            public void changed(ObservableValue o,Object oldVal, Object newVal){
                 //System.out.println("Electric bill has changed!");
                 
                 portLbl.setText(SR.getPort() + ": " + value.getPort());
                 
                 
            }
      });
        
        
        value.modeProperty().addListener(new ChangeListener(){
            @Override 
            public void changed(ObservableValue o,Object oldVal, Object newVal){
                 //System.out.println("Electric bill has changed!");
                 
                 modeLbl.setText(SR.getMode() + ": " + value.getMode());
                 
                 
            }
      });
        
        
        
    
    }
    
    
}
