/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.silverfoxserver.ext.view;

import javafx.application.Platform;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author ACER-FX
 */
public class MainWindowModel {
    
    /**
     * 打印输出
     */
    private final StringProperty outPPE;
    
    /**
     * 端口
     */
    private final StringProperty portPPE;
    
    /**
     * 模式
     */
    private final StringProperty modePPE;
    
    
    
    public MainWindowModel()
    {
        
        this.outPPE = new SimpleStringProperty("");
    
        this.portPPE = new SimpleStringProperty("");
        
        this.modePPE = new SimpleStringProperty("");
        
    }
    
    
    public StringProperty modeProperty()
    {
        return modePPE;
    }
    
    public String getMode() 
    {
	return modePPE.get();
    } 
    
    public void setMode(String value)
    {
        
        this.modePPE.set(value);
       
    }
    
    
    
    
    public StringProperty portProperty()
    {
        return portPPE;
    }
    
    public String getPort() 
    {
	return portPPE.get();
    } 
    
    public void setPort(String value)
    {
        
        this.portPPE.set(value);
       
    }
    
    
    
    
    
    public StringProperty outProperty()
    {
        return outPPE;
    }
    
    public String getOut() 
    {
	return outPPE.get();
    } 
    
    public void setOut(String value)
    {
        
        this.outPPE.set(value);
       
    }
    
    public void println(String value)
    {
        //从其它线程访问UI线程
        Platform.runLater(() -> {
            
            
        if(this.outPPE.getValueSafe().length() > 12000)//8000
        {
            this.setOut("");
        }
        
        
        this.setOut(this.outPPE.getValue() + value + "\n");
        
                            
       });
        
       
    
    }
    
}
