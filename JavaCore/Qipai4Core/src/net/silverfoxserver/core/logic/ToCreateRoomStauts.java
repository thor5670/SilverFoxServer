/*
 * SilverFoxServer: massive multiplayer game server for Flash, ...
 * VERSION:3.0
 * PUBLISH DATE:2016-2-19
 * GITHUB:github.com/wdmir/521266750_qq_com
 * UPDATES AND DOCUMENTATION AT: http://www.silverfoxserver.net
 * COPYRIGHT 2009-2015 SilverFoxServer.NET. All rights reserved.
 * MAIL:521266750@qq.com
 */
package net.silverfoxserver.core.logic;

/**
 *
 * @author FUX-hp
 */
public class ToCreateRoomStauts {
    
    // 摘要:
    //     创建成功。
    public static final int Success0 = 0;
    //
    // 摘要:
    //     只有VIP才能创建权限。
    public static final int NoVip1 = 1;
     //
    // 摘要:
    //     错误的房间的名称。
    public static final int ErrorRoomName2 = 2;
    //
    // 摘要:
    //     错误的房间的密码。
    public static final int ErrorRoomPassword3 = 3;
    //
    // 摘要:
    //     未知的错误。
    public static final int ProviderError4 = 4;
    
}
