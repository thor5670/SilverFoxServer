/*
 * SilverFoxServer: massive multiplayer game server for Flash, ...
 * VERSION:3.0
 * PUBLISH DATE:2015-9-2 
 * GITHUB:github.com/wdmir/521266750_qq_com
 * UPDATES AND DOCUMENTATION AT: http://www.silverfoxserver.net
 * COPYRIGHT 2009-2015 SilverFoxServer.NET. All rights reserved.
 * MAIL:521266750@qq.com
 */
package net.silverfoxserver.core.socket;

import java.net.InetAddress;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import net.silverfoxserver.core.log.Log;
import net.silverfoxserver.core.model.IUserModel;
import net.silverfoxserver.core.service.IoHandler;
import net.silverfoxserver.core.util.SR;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;

/**
 *
 * @author FUX
 */
public class SocketAcceptor {
    
    /**
     * 
     */
    private String _serverName = "";
    
    public String getServerName()
    {
        return _serverName;
    }
    
    /** 

    */
    private String _serverIp = "127.0.0.1";

    /** 

    */
    private int _serverPort = 9300;

    /** 

    */
    private int _maxOnlineConnection = 3000;

    /** 

    */
    private String _allowAccessFromDomain = "*";

    /** 

    */
    private String _police_xml;

    /** 

    */
    private String _police_port = "21-9399";

    /** 

    */
    private GameTcpListener _tcpLis = null;
    
    
    
    /**
     * 
     * 
     * @param GameName 
     */
    public SocketAcceptor(String GameName)
    {
          
        //
        this._serverName = GameName;
        
        _tcpLis = new GameTcpListener(GameName);
        
         //setPolice();

    }

    
    //public final void bind(String ipAdr, int port, boolean reuseAddr)
    public final void bind(int port, boolean reuseAddr)
    {

            //
            //this._serverIp = ipAdr;
            this._serverPort = port;

            //
            InetAddress ipAddress;

            
            //if (ipAdr.toLowerCase().equals("any"))
            //{
               //ipAddress = IPAddress.Any;

            //}
            //else
            //{
               //ipAddress = IPAddress.Parse(_serverIp);

            //}


            if (_tcpLis.Setup(_serverPort))
            {
//C# TO JAVA CONVERTER TODO TASK: Java has no equivalent to C#-style event wireups:
                    //_tcpLis.NewSessionConnected += new SessionHandler<AppSession>(Event_NewSessionConnected);
//C# TO JAVA CONVERTER TODO TASK: Java has no equivalent to C#-style event wireups:
                    //_tcpLis.NewRequestReceived += new RequestHandler<AppSession, StringRequestInfo>(Event_NewRequestReceived);
//C# TO JAVA CONVERTER TODO TASK: Java has no equivalent to C#-style event wireups:
                    //_tcpLis.SessionClosed += new SessionHandler<AppSession, CloseReason>(Event_SessionClosed);
                
                    if(null == this._tcpLis.getPipeline().get("SessionHandler"))
                    {
                        this._tcpLis.getPipeline().addLast("SessionHandler", new SessionHandler());
                    }
                    
                
            }
            else
            {
                    //setup failed
                    System.out.println(SR.GetString(SR.getGame_tcp_listen_setup_failed()));

                    throw new IllegalArgumentException(SR.GetString(SR.getGame_tcp_listen_setup_failed()));
            }

            //_tcpLis.Start();

            //Socket svrSocket = _tcpLis.Server;            
            //svrSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, reuseAddr);

            //Start
            //_tcpLis.Start(_maxOnlinePeople);
            
            
            _tcpLis.Start();
            
//            if (_tcpLis.Start())
//            {
//
//            }
//            else
//            {
//                    //start failed
//                    System.out.println(SR.GetString(SR.getGame_tcp_listen_start_failed()));
//
//                    throw new IllegalArgumentException(SR.GetString(SR.getGame_tcp_listen_start_failed()));
//            }

            //            
            Log.WriteStr2("------------------------------------------------------\n");
            Log.WriteStr2("Listen IP:" + _serverIp + ":" + _serverPort + "...\n");

//            if (_serverIp.equals("127.0.0.1"))
//            {
//                    //Logger.WriteStr2("提示:你的服务ip未设为外网ip");
//
//                    Log.WriteStr2(SR.GetString(SR.getip_is_not_internet()));
//            }

            if (LocalDate.now().getYear() <= 2015)
            {
                    //Logger.WriteStr2("提示:你的服务器当前时间为" + System.DateTime.Now.Year.ToString() + "年，是否正确？");

                    Log.WriteStr2(SR.GetString(SR.getserver_time_year_is_right(), 
                            String.valueOf(LocalDate.now().getYear()),
                            String.valueOf(LocalDate.now().getMonthValue())
                    ));
            }

            if (!_allowAccessFromDomain.equals("*"))
            {
                    //Logger.WriteStr2("提示:客户端只能从" + _allowAccessFromDomain + "的域名进行访问");
            }
            

            Log.WriteStr2("------------------------------------------------------\n");


    }
    
    
    /** 
	 2
	 
     @param handler        
    */
    public final void setHandler(IoHandler value,boolean vcEnable)
    {
        
        //this._handler = value;
            
        //      
         if(null == this._tcpLis.getPipeline().get("SessionHandler"))
         {
            this._tcpLis.getPipeline().addLast("SessionHandler", new SessionHandler());
         }
         
        SessionHandler h = (SessionHandler)this._tcpLis.getPipeline().get("SessionHandler");
        h.setExtHandler(value);
        h.setVcEnable(vcEnable);
        h.setSessionList(this._tcpLis.getSessionList());
        h.setSessionMapList(this._tcpLis.getSessionMapList());
      
    }
    
    /** 
	 
	 
     @param strIpPort
     @return 
    */
    public final AppSession getSession(String strIpPort)
    {       
        return this._tcpLis.GetAppSessionByID(strIpPort);

    }
    
    /** 


     @param strIpPort
     @return 
    */
    public final boolean hasSession(String strIpPort)
    {
            return this._tcpLis.hasAppSessionByID(strIpPort);

    }
    
    /** 
     向外界提供的clearSession方法，为触发制
     在这里必须说明，能调用该函数的情况：1.重复登录，把对方挤下线

     @param strIpPort
    */
    public final void trigClearSession(AppSession session, String strIpPort)
    {
        
        
        if(null != session && 
           null != session.getChannel()){//提高性能
            
            session.getChannel().close();
        
        }
        else if (this.hasSession(strIpPort))
        {
                //this.getSession(strIpPort).Close();
            this.getSession(strIpPort).getChannel().close();
        }


    }
    
    

    
        
}
