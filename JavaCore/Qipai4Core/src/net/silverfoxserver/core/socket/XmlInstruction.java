/*
 * SilverFoxServer: massive multiplayer game server for Flash, ...
 * VERSION:3.0
 * PUBLISH DATE:2015-9-2 
 * GITHUB:github.com/wdmir/521266750_qq_com
 * UPDATES AND DOCUMENTATION AT: http://www.silverfoxserver.net
 * COPYRIGHT 2009-2015 SilverFoxServer.NET. All rights reserved.
 * MAIL:521266750@qq.com
 */
package net.silverfoxserver.core.socket;

import System.Xml.XmlDocument;
import java.io.UnsupportedEncodingException;
import org.jboss.netty.util.CharsetUtil;

/**
 *
 * @author ACER-FX
 */
public class XmlInstruction {
    
        /**
         * 序列化XML为字节发送序列
         * 
         * @param value
         * @return 
         * @throws java.io.UnsupportedEncodingException 
         */
        public static byte[] Serializer(XmlDocument value) throws UnsupportedEncodingException
        {
            
            String s = value.toString()  + "\0";
            
            return s.getBytes(CharsetUtil.UTF_8);//"UTF-8");
        
        }
    
    
    /** 
	 封包
	 
	 @return 
	*/

        @Deprecated
	public static byte[] fengBao(String action, String content) throws UnsupportedEncodingException
	{
		//最后加\0
		String s = "<msg t='sys'><body action='" + action + "'>" + content + "</body></msg>\0";

		//return Encoding.UTF8.GetBytes(resXml);
                return s.getBytes(CharsetUtil.UTF_8);//"UTF-8");
	}
        
        @Deprecated
        public static byte[] fengBao(String action) throws UnsupportedEncodingException
	{
		//最后加\0
		String s = "<msg t='sys'><body action='" + action + "'>" + "" + "</body></msg>\0";

		//return Encoding.UTF8.GetBytes(resXml);
                return s.getBytes(CharsetUtil.UTF_8);//"UTF-8");
	}

	/** 
	 封包ByDB
	 
	 @return 
	*/

        
        @Deprecated
	public static byte[] DBfengBao(String action, String content) throws UnsupportedEncodingException
	{
		//最后加\0
		//DBS = data base system
		String s = "<msg t='DBS'><body action='" + action + "'>" + content + "</body></msg>\0";
                
		//return Encoding.UTF8.GetBytes(resXml);
                return s.getBytes("UTF-8");
	}
        
        
        /**
         * 创建XML的模板
         * 
         * 结尾不要加\0
         * 否则会引发 Error on line 1: 尾随节中不允许有内容 的异常
         */
        public final static String SysTemplate = "<msg t='sys'><body action='" + "" + "'>" + "" + "</body></msg>";

    
}
