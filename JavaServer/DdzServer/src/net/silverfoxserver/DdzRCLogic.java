/*
 * SilverFoxServer: massive multiplayer game server for Flash, ...
 * VERSION:3.0
 * PUBLISH DATE:2015-9-2 
 * GITHUB:github.com/wdmir/521266750_qq_com
 * UPDATES AND DOCUMENTATION AT: http://www.silverfoxserver.net
 * COPYRIGHT 2009-2015 SilverFoxServer.NET. All rights reserved.
 * MAIL:521266750@qq.com
 */
package net.silverfoxserver;

import System.Xml.XmlDocument;
import System.Xml.XmlNode;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.silverfoxserver.core.log.Log;
import net.silverfoxserver.core.model.IUserModel;
import net.silverfoxserver.core.protocol.RCClientAction;
import net.silverfoxserver.core.protocol.ServerAction;
import net.silverfoxserver.core.server.GameLogicServer;
import net.silverfoxserver.core.server.RecordLogicClientServer;
import net.silverfoxserver.core.socket.AppSession;
import net.silverfoxserver.core.socket.SessionMessage;
import net.silverfoxserver.core.socket.XmlInstruction;
import net.silverfoxserver.core.util.SR;
import net.silverfoxserver.extfactory.UserModelFactory;
import net.silverfoxserver.handler.DdzGateClientHandler;
import org.jdom2.Element;
import org.jdom2.JDOMException;

/**
 *
 * @author FUX
 */
public class DdzRCLogic extends RecordLogicClientServer {
    
    /**
     * 
     * @return 
     */
    @Override
    public String CLASS_NAME()
    {            
        
        return DdzRCLogic.class.getName();                 
    }
    
    /**
     * 单例
     * 
     */
    private static DdzRCLogic _instance = null;
    
    public static DdzRCLogic getInstance()
    {
        if(null == _instance)
        {
            _instance = new DdzRCLogic();
        }
    
        return _instance;
    }
    
    public void init(GameLogicServer value)
    {
        super.setGameLogicServer(value);
    
    }
    
    //C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
            ///#endregion

//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
            ///#region 数据库服务协议处理入口

    @Override
    public void doorLogOK(AppSession session, XmlDocument doc, SessionMessage item)
    {
            try
            {
                    XmlNode node = doc.SelectSingleNode("/msg/body");

                    String userStrIpPort = node.getAttributeValue("s");
                    String gateStrIpPort = node.getAttributeValue("fromGate");//node.ChildNodes()[0].getText();
                    
                    String usersex     = node.getChildNode("sex").getText();//node.ChildNodes()[1].getText();
                    String username = node.getChildNode("nick").getText();//node.ChildNodes()[2].getText();
                    String userpwd = node.getChildNode("pword").getText();//node.ChildNodes()[3].getText();
                    String useremail   = node.getChildNode("email").getText();//node.ChildNodes()[4].getText();
                    String bbs = node.getChildNode("bbs").getText();//node.ChildNodes()[5].getText();
                    String hico = node.getChildNode("hico").getText();//node.ChildNodes()[6].getText();
                    String sid = node.getChildNode("sid").getText();//node.ChildNodes()[7].getText();
                    int id_sql = parseInt(node.getChildNode("id_sql").getText());//Integer.valueOf(node.ChildNodes()[8].getText());

                    String id = node.getChildNode("id").getText();//node.ChildNodes()[9].getText();
                    String status = node.getChildNode("login_status").getText();//node.ChildNodes()[10].getText();


                    //
                    String saction = ServerAction.logOK;
                    //StringBuilder contentXml = new StringBuilder();
                    

                    //回复
                    //注意这里的session是上面的usersession，要判断是否还在线
                    AppSession userSession = DdzLogic.getInstance().CLIENTAcceptor.getSession(gateStrIpPort);

                    //判断重复登录,如果这里发生异常，可能就需要多登录几次才能挤掉对方，并成功登录
                    AppSession outSession = null;//DdzLogic.getInstance().CLIENTAcceptor.getSessionByAccountName(username);

                    if (null != outSession)
                    {
                            //发一个通知，您与服务器的连接断开，原因：您的帐号在另一处登录
                            //然后触发ClearSession
                            String logoutAction = ServerAction.logout;
                            String logoutCode = "1";
                            StringBuilder logoutXml = new StringBuilder();

                            logoutXml.append("<session>").append(userSession.getRemoteEndPoint().toString()).append("</session>");
                            logoutXml.append("<session>").append(outSession.getRemoteEndPoint().toString()).append("</session>");
                            logoutXml.append("<code>").append(logoutCode).append("</code>");
                            logoutXml.append("<u></u>");

                            DdzLogic.getInstance().Send(outSession, XmlInstruction.fengBao(logoutAction, logoutXml.toString()));

                            //
                            Log.WriteStrBySend(logoutAction, outSession.getRemoteEndPoint().toString());

                            //
                            DdzLogic.getInstance().CLIENTAcceptor.trigClearSession(outSession, outSession.getRemoteEndPoint().toString());
                    }

                    //如果不在线则略过发送
                    if (null != userSession)
                    {
                            //超过在线人数
                                    if (DdzLogic.getInstance().UserManager.getUserCount() >= DdzLogic.getInstance().UserManager.getMaxOnlineUserConfig())
                                    {
                                            //调整saction
                                            saction = ServerAction.logKO;
                                            //调整status
                                            status = "12"; //来自MembershipLoginStatus2.PeopleFull12
                                            
                                            doc.getDocumentElement().getChildren().get(0).removeContent();

                                            //contentXml.append("<session>").append(userStrIpPort).append("</session>");
                                            //contentXml.append("<status>").append(status).append("</status>");
                                            //contentXml.append("<u></u>");
                                            
                                            Element statusElement = new Element("status");
                                            statusElement.setText(status);
                                            
                                            doc.getDocumentElement().getChildren().get(0).addContent(status);
                                            doc.getDocumentElement().getChildren().get(0).addContent(new Element("u"));

                                            DdzLogic.getInstance().Send(userSession, 
                                                    
                                                    XmlInstruction.Serializer(doc)
                                                    //XmlInstruction.fengBao(saction, contentXml.toString()
                                                    
                                                    
                                                    );

                                            //
                                            Log.WriteStrBySend(saction, userStrIpPort);
                                    }
                                    else
                                    {
                                            IUserModel user = UserModelFactory.Create(userStrIpPort, 
                                                    gateStrIpPort,
                                                    id, id_sql, usersex, username, username, bbs, hico);

                                            //加入在线用户列表
                                            //CLIENTAcceptor.addUser(userSession.getRemoteEndPoint().ToString(), user);
                                            DdzLogic.getInstance().logicAddUser(userStrIpPort, user);

                                            doc.getDocumentElement().getChildren().get(0).removeContent();
                                            
                                            //contentXml.append("<session>").append(userStrIpPort).append("</session>");
                                            //contentXml.append("<status>").append(status).append("</status>");
                                            //contentXml.append(user.toXMLString());
                                            
                                             Element statusElement = new Element("status");
                                            statusElement.setText(status);
                                            
                                            doc.getDocumentElement().getChildren().get(0).addContent(status);
                                            doc.getDocumentElement().getChildren().get(0).addContent(user.toElement());


                                            DdzLogic.getInstance().Send(userSession, 
                                                    
                                                    XmlInstruction.Serializer(doc)
                                                    //XmlInstruction.fengBao(saction, contentXml.toString())
                                            );

                                            //
                                            Log.WriteStrBySend(saction, userStrIpPort,username);

                                            //
                                            //Log.WriteFileByLoginSuccess(username, userStrIpPort);
                                            Log.WriteFileByOnlineUserCount(DdzLogic.getInstance().UserManager.getUserCount());

                                    } //end if

                    } //end if



            }
            catch (JDOMException | UnsupportedEncodingException  | RuntimeException exd)
            {
                    Log.WriteStrByException(CLASS_NAME(), "doorLogOK", exd.getMessage(),exd.getStackTrace());
            }
    }    
    
    @Override
    public void doorLoadGOK(AppSession session, XmlDocument doc, SessionMessage item)
    {
            try
            {
                    XmlNode node = doc.SelectSingleNode("/msg/body");

                    String userStrIpPort = node.getAttributeValue("s");//node.ChildNodes()[0].getText();//InnerText;

                    String gateStrIpPort = node.getAttributeValue("fromGate");
                    
                    String gateName = node.getAttributeValue("gate");
                    
                    String g = node.getChildNode("u").getAttributeValue("g");//node.ChildNodes()[1].getText();//InnerText;

                    //String id_sql = node.ChildNodes()[1].getAttributeValue("id_sql");
                    //
                    String sAction = ServerAction.gOK;
                    
                    doc.getBodyElement().getAttribute("action").setValue(sAction);
                    
                    
                    //回复
                    //注意这里的session是上面的usersession，要判断是否还在线
                    AppSession gateSession = getGameLogicServer().netGetSession(gateStrIpPort);
                    
                    //对于断线重连造成的fromGate失效问题，增强稳定性
                    if(null == gateSession)
                    {
                        String gateStrIpPortNow = DdzGateClientHandler._gateMapList.get(gateName);
                        gateSession = getGameLogicServer().netGetSession(gateStrIpPortNow);
                    }
                    

                    //如果不在线则略过发送
                    if (null != gateSession)
                    {
                        IUserModel u = getGameLogicServer().UserManager.getUser(userStrIpPort);

                        //以免引发不必要的异常，方便测试
                        if (!g.equals(""))
                        {
                             u.setG(Integer.parseInt(g));
                        }
										
                        getGameLogicServer().Send(gateSession, 
                                
                                XmlInstruction.Serializer(doc)
                                //XmlInstruction.fengBao(saction, 
                                //"<g " + "id_sql='" + id_sql + "'" + ">" + g + "</g>")
                        );

                        //
                        Log.WriteStrBySend(sAction, userStrIpPort,u.getAccountName(),gateSession.getRemoteEndPoint().toString());


                    }
                    else
                    {
                        Log.WriteStrBySendFailed(sAction, gateStrIpPort);
                    } //end if

            }
            catch (JDOMException | UnsupportedEncodingException | RuntimeException exd)
            {
                    Log.WriteStrByException(CLASS_NAME(), "doorLoadGOK", exd.getMessage(),exd.getStackTrace());
            }
    }    


}
