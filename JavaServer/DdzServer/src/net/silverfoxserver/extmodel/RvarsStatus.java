/*
 * SilverFoxServer: massive multiplayer game server for Flash, ...
 * VERSION:3.0
 * PUBLISH DATE:2015-9-2 
 * GITHUB:github.com/wdmir/521266750_qq_com
 * UPDATES AND DOCUMENTATION AT: http://www.silverfoxserver.net
 * COPYRIGHT 2009-2015 SilverFoxServer.NET. All rights reserved.
 * MAIL:521266750@qq.com
 */
package net.silverfoxserver.extmodel;

/**
 *
 * @author FUX
 */
public class RvarsStatus {
    
    public static final int Success0 = 0;
    
    /**
     * 只能由是本人申请本人的 
     * 只能由我自已对自已的数据进行操作
     * 
     * I can only apply for my own
     */
    public static final int CanOnlyApplyForMyOwn1 = 1;
    
    /**
     * 没有这张牌
     */
    public static final int NoCard2 = 2;
    
    /**
     * 叫分不能超过3分
     */
    public static final int ExceededJfMaxValue3 = 3;
    
    
    
    
   
    
}
